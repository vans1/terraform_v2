output "access_key" {
  value = yandex_iam_service_account_static_access_key.vans-static-key.access_key
  sensitive = true
}
output "secret_key" {
  value = yandex_iam_service_account_static_access_key.vans-static-key.secret_key
  sensitive = true
}

output "internal_ip_address_vm-01" {
  value = module.instance_1.internal_ip_address_vm-01
}

output "external_ip_address_vm-01" {
  value = module.instance_1.external_ip_address_vm-01
}

output "internal_ip_address_vm-02" {
  value = module.instance_2.internal_ip_address_vm-02
}

output "external_ip_address_vm-02" {
  value = module.instance_2.external_ip_address_vm-02
}
