# YANDEX PROVIDER

terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.78.1" #fix provider version
    }
  }

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "sf-bucket"
    region     = "ru-central1-a"
    key        = "issue1/terraform.tfstate"
    access_key = "YCAJEOtTbVq2T4SwIvlZoWUCN"
    secret_key = "YCNxqcMrtSIITQimnxdr_nsp4l63kKt29pO2bQmv"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = "ru-central1-a"
}


#BUCKET
// Create SA
  resource "yandex_iam_service_account" "vans" {
  folder_id = var.folder_id
  name      = "vans"
}

resource "yandex_resourcemanager_folder_iam_member" "vans" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.vans.id}"
}

// Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "vans-static-key" {
  service_account_id = yandex_iam_service_account.vans.id
  description        = "static access key for object storage"
}

// Use keys to create bucket
resource "yandex_storage_bucket" "test" {
  access_key = yandex_iam_service_account_static_access_key.vans-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.vans-static-key.secret_key
  bucket = "sf-bucket"
}

# RESOURCES

resource "yandex_vpc_network" "sf-network" {
  name = "sf-network"
}

resource "yandex_vpc_subnet" "subnet-sf-01" {
  name           = "subnet-sf-01"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.sf-network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "subnet-sf-02" {
  name           = "subnet-sf-02"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.sf-network.id
  v4_cidr_blocks = ["192.168.20.0/24"]
}

module "instance_1" {
  source                = "./modules/instance"
  instance_family_image = "lemp"
  vpc_subnet_id         = yandex_vpc_subnet.subnet-sf-01.id
#  zone                  = "ru-central1-a"
}

module "instance_2" {
  source                = "./modules/instance"
  instance_family_image = "lamp"
  vpc_subnet_id         = yandex_vpc_subnet.subnet-sf-02.id
#  zone                  = "ru-central1-b"
}
